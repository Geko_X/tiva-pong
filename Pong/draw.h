
#define MAX_X			128
#define	MAX_Y			63

void dispDec(int dec);

void drawField(void);

void fillScreen(void);
void clearScreen(void);

void drawScore(int score1, int score2);

void clearBall(int x, int y);
void drawBall(int x, int y);

void clearPaddle(int player, int y);
void drawPaddle(int player, int y);
