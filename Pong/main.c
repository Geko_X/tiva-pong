#include "lcd.h"
#include "draw.h"
#include "pong.h"

// Main
//
// Main loop is done here, as well as all setups and interrupts

/******************** DEFINE ***************************/

#define SYSTICK_BASE		(*((unsigned long *)0xE000E000))
#define NVIC_ST_CTRL		(*((unsigned long *)0xE000E010))
#define	NVIC_ST_RELOAD	(*((unsigned long *)0xE000E014))
#define NVIC_ST_CURRENT	(*((unsigned long *)0xE000E018))
#define NVIC_ST_PRI3		(*((unsigned long *)0xE000ED20))

#define GPIO_B_BASE			(*((unsigned long *)0x40005000))
#define	GPIO_DATA				(*((unsigned long *)0x400053FC))
#define	GPIO_DIR				(*((unsigned long *)0x40005400))
#define	GPIO_AFSEL			(*((unsigned long *)0x40005420))
#define	GPIO_DEN				(*((unsigned long *)0x4000551C))
#define	GPIO_AMSEL			(*((unsigned long *)0x40005528))
#define	GPIO_DR8R				(*((unsigned long *)0x40005508))
#define GPIO_PUR				(*((unsigned long *)0x40005510))
#define GPIO_PDR				(*((unsigned long *)0x40005514))

#define RCGCGPIO				(*((unsigned long *)0x400FE608))
#define	PRGPIO					(*((unsigned long *)0x400FEA08))
	
#define RCGCADC					(*((unsigned long *)0x400FE638))
#define PRADC						(*((unsigned long *)0x400FEA38))
	
#define ADC0_BASE				(*((unsigned long *)0x40038000))

#define ADC0_SSPRI			(*((unsigned long *)0x40038020))
#define ADC0_ADCPP			(*((unsigned long *)0x40038FC0))
#define ADC0_EMUX				(*((unsigned long *)0x40038014))
#define ADC0_SSMUX3			(*((unsigned long *)0x400380A0))
#define ADC0_SSCTL3			(*((unsigned long *)0x400380A4))
#define ADC0_PSSI				(*((unsigned long *)0x40038028))
#define ADC0_RIS				(*((unsigned long *)0x40038004))
#define ADC0_SSFIFO3		(*((unsigned long *)0x400380A8))
#define ADC0_ISC				(*((unsigned long *)0x4003800C))
#define ADC0_IM					(*((unsigned long *)0x40038008))

#define ADC1_BASE				(*((unsigned long *)0x40039000))
	
#define ADC1_SSPRI			(*((unsigned long *)0x40039020))
#define ADC1_PSSI				(*((unsigned long *)0x40039028))
#define ADC1_EMUX				(*((unsigned long *)0x40039014))
#define ADC1_SSMUX3			(*((unsigned long *)0x400390A0))
#define ADC1_SSCTL3			(*((unsigned long *)0x400390A4))
#define ADC1_RIS				(*((unsigned long *)0x40039004))
#define ADC1_SSFIFO3		(*((unsigned long *)0x400390A8))
#define ADC1_ISC				(*((unsigned long *)0x4003900C))
#define ADC1_ADCPP			(*((unsigned long *)0x40039FC0))
#define ADC1_IM					(*((unsigned long *)0x40039008))

#define PORT_MASK_B			0x02
#define POT_1_MASK			0x10			// Pin B4
#define POT_2_MASK			0x20			// Pin B5

#define RELOAD					0x4C4B39

/****************** PROTOTYPES **************************/

void initSysTick(void);
void initGPIOF(void);
void __irq STH(void);
void initACD(void);
void __irq adc0(void);
void __irq adc1(void);
int getADC(int adc);

/********************* MAIN ****************************/
int main(void) {	

	InitLCD();
	
	initACD();
	initSysTick();
	
	//Backlight(7);
	
	// Start the game
	pongStart();
	
	while (1) __asm("nop") ; // end spin (should never pass here);

}

/********************* SYSTICK ****************************/

void initSysTick(void) {
	
	NVIC_ST_CTRL &= ~0x1;
	NVIC_ST_RELOAD = RELOAD;
	NVIC_ST_CURRENT = 0;
	NVIC_ST_CTRL |= 0x7;
	
}

// SysTick interrupt
void __irq STH(void) {
	setPaddle(PLAYER_1, ADC0_SSFIFO3);
	setPaddle(PLAYER_2, ADC1_SSFIFO3);
	pongTick();
}

/********************* ADC ****************************/

void initACD(void) {
	
	// Enable clock for GPIO B
	RCGCGPIO |= PORT_MASK_B;
	while((PRGPIO & PORT_MASK_B) != PORT_MASK_B) {
		__asm("nop");
	}
	
	// Setup ACD0 for player 1 and ACD1 for player 2
	RCGCADC |= 0x3;			// 0011
	while((PRADC & 0x3) != 0x3) {
		__asm("nop");
	}
	
	// Set AFSEL bit
	GPIO_AFSEL |= POT_1_MASK;
	GPIO_AFSEL |= POT_2_MASK;
	
	// Clear for input
	GPIO_DIR &= ~POT_1_MASK;
	GPIO_DEN &= ~POT_1_MASK;
	GPIO_DIR &= ~POT_2_MASK;
	GPIO_DEN &= ~POT_2_MASK;
	
	// Enable analog
	GPIO_AMSEL |= POT_1_MASK;
	GPIO_AMSEL |= POT_2_MASK;
	
	// Disable sequencer
	ADC0_BASE &= ~0x8;
	ADC1_BASE &= ~0x8;
	
	// Set sampling rate
	ADC0_ADCPP = 0x1;
	ADC1_ADCPP = 0x1;

	// Set priority
	ADC0_SSPRI |= (0x1000);
	
	// Set trigger to continuous
	ADC0_EMUX |= 0xF000;
	ADC1_EMUX |= 0xF000;
	
	// Input source
	ADC0_SSMUX3 = 10;	// AIN 10
	ADC1_SSMUX3 = 11;	// AIN 11
	
	// Control bits
	ADC0_SSCTL3 = 0x6;
	ADC1_SSCTL3 = 0x6;
	
	// Enable sequencer
	ADC0_BASE |= 0x8;
	ADC1_BASE |= 0x8;
	
	// CLear interrupts just in case
	ADC0_ISC |= 0x8;
	ADC1_ISC |= 0x8;
	
}

void __irq adc0(void) {
	ADC0_ISC |= 0x8;
}

void __irq adc1(void) {
	ADC1_ISC |= 0x8;
}

