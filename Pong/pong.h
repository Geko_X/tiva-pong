
#define PLAYER_1			1
#define PLAYER_2			2

#define BALL_SIZE			3
#define PADDLE_SIZE	 	16
#define PADDLE_WIDTH	2
#define PADDLE_OFFSET	5

#define GAME_WIN			5

void pongStart(void);
void pongTick(void);

int moveBall(void);
void setPaddle(int player, int pot);

void pause(int ticks);
