#include "draw.h"
#include "pong.h"
#include "lcd.h"

// All game logic goes here

// Current game tick
int currentTick = 0;

// If this is > 0, the game will "pause" and tick this down
int pauseTicks = 0;

// Ball
int ballX, ballY;
int ballVX, ballVY;

// Paddles
// Just the Y position is stored
int paddle1, paddle2;

// Min and max paddle positions
int paddleMax = 54;
int paddleMin = 10;

// Scores
int scorePlayer1 = 0;
int scorePlayer2 = 0;

int checkBallMove(int currentX, int currentY, int velocityX, int velocityY);
void resetField(void);
void resetGame (void);
int pot2screen(int pot);

/****************** GAME TICK *************************/

void pongStart() {
	
	paddle1 = 0;
	paddle2 = 0;
	
	drawField();
	drawScore(0, 0);
	
	resetField();
	
	drawBall(ballX, ballY);
	drawPaddle(PLAYER_1, paddle1);
	drawPaddle(PLAYER_2, paddle2);
	
	
}

void pongTick(void) {
	
	int result;
	
	currentTick++;
	
	if(pauseTicks > 0) {
		pauseTicks--;
		return;
	}
	
	
	// Check win
	if(scorePlayer1 == GAME_WIN) {
		resetGame();
	}
	
	if(scorePlayer2 == GAME_WIN) {
		resetGame();
	}

	// Move ball
	result = moveBall();
	
	if(result == PLAYER_2) {
		scorePlayer1++;
		resetField();
	}
	
	if(result == PLAYER_1) {
		scorePlayer2++;
		resetField();
	}
	
	// Draw stuff
	drawPaddle(PLAYER_1, paddle1);
	drawPaddle(PLAYER_2, paddle2);

	drawField();
	drawScore(scorePlayer1, scorePlayer2);
	
}

void resetField() {
	
	pause(20);
	
	ballX = MAX_X / 2;
	ballY = MAX_Y / 2;
	ballVX = 1;
	ballVY = 1;
	
}

void resetGame() {
	
	scorePlayer1 = 0;
	scorePlayer2 = 0;
	
	fillScreen();
	clearScreen();
	
	resetField();
}


/******************** BALL ***************************/

// Checks if the ball can move to the target place
//
// Returns 0 if no collision
// Returns 1 if collision on top/bottom
// Returns 2 if collision on the left/right
// Returns 3 if hit by a paddle
int checkBallMove(int currentX, int currentY, int velocityX, int velocityY) {
	
	// Top/bottom
	if((currentY + velocityY >= MAX_Y - BALL_SIZE / 2) || (currentY + velocityY < BALL_SIZE)) {
		return 1;
	}
	
	// Left/right
	if((currentX + velocityX >= MAX_X - BALL_SIZE / 2) || (currentX + velocityX < BALL_SIZE)) {
		return 2;
	}
	
	// Paddles
	// Left paddle (player 1)
	if((currentX + velocityX <= PADDLE_OFFSET) && (currentY + velocityY < paddle1 + (PADDLE_SIZE / 2) + 1) && (currentY + velocityY > paddle1 - (PADDLE_SIZE / 2) - 1)) {
		return 3;
	}
	
	// Right paddle (player 2)
	if((currentX + velocityX >= MAX_X - PADDLE_OFFSET) && (currentY + velocityY < paddle2 + (PADDLE_SIZE / 2) + 1) && (currentY + velocityY > paddle2 - (PADDLE_SIZE / 2) - 1)) {
		return 3;
	}
	
	
	// Nothing (can move here)
	return 0;
}

// Moves the ball
int moveBall() {
	
	int collision = checkBallMove(ballX, ballY, ballVX, ballVY);
	
	clearBall(ballX, ballY);
	
	// No collision
	if(collision == 0) {
		__asm("nop");
	}
	
	// Top/bottom
	if(collision == 1) {
		ballVY *= -1;
	}
	
	// Left/right (in the score zone)
	//
	// Return the PLAYER who scored
	if(collision == 2) {
		ballVX *= -1;
		
		if(ballX < MAX_X / 2)
			return PLAYER_1;
		
		else
			return PLAYER_2;
		
	}
	
	// Hit a paddle
	if(collision == 3) {
		ballVX *= -1;
	}
	
	ballX += ballVX;
	ballY += ballVY;
	
	drawBall(ballX, ballY);
	
	return 0;
	
}

/******************** PADDLE ***************************/

// Sets a paddle's position with a pot value
void setPaddle(int player, int pot) {
	
	if(player == PLAYER_1) {
		clearPaddle(PLAYER_1, paddle1);
		paddle1 = pot2screen(pot);
	}
	
	if(player == PLAYER_2) {
		clearPaddle(PLAYER_2, paddle2);
		paddle2 = pot2screen(pot);
	}
}

/******************** HELPER STUFF ***************************/

// Converts a value from the pots to a screenspace value
int pot2screen(int pot) {

	// One yPixel is 1/MAX_Y,
	// but there is space we cannot use (determined from paddleMin/Max),
	// so one yPixel is 1/(MAX_Y - paddleMin - (MAX_Y - paddleMax)
	//									1/( 63	 - 		10 		 - ( 63		-   54)
	//									1/44
	// This means that for each pixel, we need to read an aditional 1/44%
	// voltage from the pots, ie: each 2.27% of 4095 is 1 pixel
	//
	// 2.27% of 4095 is 93
	//
	// 93 is the magic number
	
	int screen = pot / 92;
	screen += paddleMin;
	
	// Clamp the value, just in case
	screen = screen < paddleMin ? paddleMin : screen > paddleMax ? paddleMax : screen;
	
	return screen;
}

// Delay for a given time
void pause(int ticks) {
	pauseTicks = ticks;
}
