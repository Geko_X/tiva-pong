#include "lcd.h"
#include "draw.h"
#include "pong.h"

#define P2_OFFSET 4;

// All drawing fuctions go here

/********************* HELERPS ****************************/

void dispDec(int dec) {
	
	int t, o;
	
	t = dec / 10;
	o = dec - (t * 10);

	t += 48;
	o += 48;
	
	PutcharLCD((char) t);
	PutcharLCD((char) o);
}

void drawField() {
	
	int x, y;
	
	// Top and bottom
	for(x = 0; x <= MAX_X; x++) {
		DrawPixel(x, 0);
		DrawPixel(x, 1);
		DrawPixel(x, MAX_Y);
		DrawPixel(x, MAX_Y - 1);
	}
	
	// Center line
	for(y = 0; y <= MAX_Y; y++) {
		if(y % 2 == 0)
			DrawPixel(MAX_X / 2, y);
	}
	
}

void fillScreen() {
	int x = 0;
	int y = 0;
	
	for(x = 0; x < MAX_X; x++) {
		for(y = 0; y < MAX_Y; y++) {
			DrawPixel(x, y);
		}
	}
}

void clearScreen() {
	int x = 0;
	int y = 0;
	
	for(x = 0; x < MAX_X; x++) {
		for(y = 0; y < MAX_Y; y++) {
			ClearPixel(x, y);
		}
	}
}

// Draws the scores for a both players
void drawScore(int score1, int score2) {
	
	int x = 8;
	int y = 1;
	
	CursorPos(x, y);
	PutcharLCD(48 + score1);		// 48 is 0
	
	x += P2_OFFSET
	CursorPos(x, y);
	PutcharLCD(48 + score2);		// 48 is 0
	
}

void clearBall(int x, int y) {
	
	int i, j;
	for(i = x - BALL_SIZE / 2; i <= x + BALL_SIZE / 2; i++) {
		for(j = y - BALL_SIZE / 2; j <= y + BALL_SIZE / 2; j++) {
			ClearPixel(i, j);
		}
	}
}

// Draws the ball
void drawBall(int x, int y) {
	
	int i, j;
	for(i = x - BALL_SIZE / 2; i <= x + BALL_SIZE / 2; i++) {
		for(j = y - BALL_SIZE / 2; j <= y + BALL_SIZE / 2; j++) {
			DrawPixel(i, j);
		}
	}
}

// Paddles
void clearPaddle(int player, int y) {
	int i, j;
	int x;
	int top, bottom, left, right;
	
	top = y - PADDLE_SIZE / 2;
	bottom = y + PADDLE_SIZE / 2;
	
	x = player == PLAYER_1 ? PADDLE_OFFSET : MAX_X - PADDLE_OFFSET;
	
	left = x - PADDLE_WIDTH / 2;
	right = x + PADDLE_WIDTH / 2;
	
	for(i = left; i <= right; i++) {
		for(j = top; j <= bottom; j++) {
			ClearPixel(i, j);
		}
	}
}

void drawPaddle(int player, int y) {
	int i, j;
	int x;
	int top, bottom, left, right;
	
	top = y - PADDLE_SIZE / 2;
	bottom = y + PADDLE_SIZE / 2;
	
	x = player == PLAYER_1 ? PADDLE_OFFSET : MAX_X - PADDLE_OFFSET;
	
	left = x - PADDLE_WIDTH / 2;
	right = x + PADDLE_WIDTH / 2;
	
	for(i = left; i <= right; i++) {
		for(j = top; j <= bottom; j++) {
			DrawPixel(i, j);
		}
	}
}
