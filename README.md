# README #

Pong, written for a TivaC microcontroller.

The Tiva has an attached LCD screen and 2 potentiometers.

## Files ##

 - main.c:
 	- The start point of the program, as well as containing all uC code
	- Runs the main loop of the program
	
 - pong.c
 	- Game code and logic is here
	- Called by main.c every update tick
	
 - draw.c
 	- All drawing functions are here
	- Wrapper and some convience code for the LCD library
 